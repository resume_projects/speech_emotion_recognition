import numpy as np
import pandas as pd
import os
from keras.models import model_from_json
import librosa
import keras
from flask import Flask, request, Response
from flask_cors import CORS, cross_origin


opt = keras.optimizers.RMSprop(lr=0.00001, decay=1e-6)

# loading json and creating model
json_file = open('model.json', 'r')
loaded_model_json = json_file.read()
json_file.close()
loaded_model = model_from_json(loaded_model_json)

# load weights into new model
loaded_model.load_weights("./Emotion_Voice_Detection_Model.h5")
print("Loaded model from disk")

# evaluate loaded model on test data
loaded_model.compile(loss='categorical_crossentropy',
                     optimizer=opt, metrics=['accuracy'])

app = Flask(__name__)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'


def root_dir():  # pragma: no cover
    return os.path.abspath(os.path.dirname(__file__))


def get_file(filename):  # pragma: no cover
    try:
        src = os.path.join(root_dir(), filename)
        # Figure out how flask returns static files
        # Tried:
        # - render_template
        # - send_file
        # This should not be so non-obvious
        return open(src).read()
    except IOError as exc:
        return str(exc)

@app.route('/')
def man():
    content = get_file('index.html')
    return Response(content, mimetype="text/html")


@app.route('/predict', methods=['POST'])
@cross_origin()
def home():
    X, sample_rate = librosa.load(
        request.files.get('data'), res_type='kaiser_fast', duration=2.5, sr=22050*2, offset=0.5)
    sample_rate = np.array(sample_rate)
    mfccs = np.mean(librosa.feature.mfcc(
        y=X, sr=sample_rate, n_mfcc=13), axis=0)
    featurelive = mfccs
    livedf2 = featurelive
    livedf2 = pd.DataFrame(data=livedf2)
    livedf2 = livedf2.stack().to_frame().T
    twodim = np.expand_dims(livedf2, axis=2)
    livepreds = loaded_model.predict(twodim, batch_size=32, verbose=1)
    livepreds1 = livepreds.argmax(axis=1)
    liveabc = livepreds1.astype(int).flatten()

    if (liveabc[0] == 0):
        return 'Female Angry'
    elif (liveabc[0] == 1):
        return 'Female Calm'
    elif (liveabc[0] == 2):
        return 'Female Fearful'
    elif (liveabc[0] == 3):
        return 'Female Happy'
    elif (liveabc[0] == 4):
        return 'Female Sad'
    elif (liveabc[0] == 5):
        return 'Male Angry'
    elif (liveabc[0] == 6):
        return 'Male Calm'
    elif (liveabc[0] == 7):
        return 'Male Fearful'
    elif (liveabc[0] == 8):
        return 'Male Happy'
    elif (liveabc[0] == 9):
        return 'Male Sad'

    #return str(liveabc[0])
    


if __name__ == "__main__":
    from waitress import serve
    serve(app, host="0.0.0.0", port=8080)
